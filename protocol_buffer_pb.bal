import ballerina/grpc;
import ballerina/protobuf.types.wrappers;

public isolated client class assessmentManagementClient {
    *grpc:AbstractClientEndpoint;

    private final grpc:Client grpcClient;

    public isolated function init(string url, *grpc:ClientConfiguration config) returns grpc:Error? {
        self.grpcClient = check new (url, config);
        check self.grpcClient.initStub(self, ROOT_DESCRIPTOR_PROTOCOL_BUFFER, getDescriptorMapProtocolBuffer());
    }

    isolated remote function assign_courses(CreateCourseRequest|ContextCreateCourseRequest req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        CreateCourseRequest message;
        if req is ContextCreateCourseRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("assignment01P01.assessmentManagement/assign_courses", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function assign_coursesContext(CreateCourseRequest|ContextCreateCourseRequest req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        CreateCourseRequest message;
        if req is ContextCreateCourseRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("assignment01P01.assessmentManagement/assign_courses", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function view_results(ViewRequest|ContextViewRequest req) returns ViewResponse|grpc:Error {
        map<string|string[]> headers = {};
        ViewRequest message;
        if req is ContextViewRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("assignment01P01.assessmentManagement/view_results", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <ViewResponse>result;
    }

    isolated remote function view_resultsContext(ViewRequest|ContextViewRequest req) returns ContextViewResponse|grpc:Error {
        map<string|string[]> headers = {};
        ViewRequest message;
        if req is ContextViewRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("assignment01P01.assessmentManagement/view_results", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <ViewResponse>result, headers: respHeaders};
    }

    isolated remote function request_assignments(SubmitAssignmentRequest|ContextSubmitAssignmentRequest req) returns SubmitAssignmentResponse|grpc:Error {
        map<string|string[]> headers = {};
        SubmitAssignmentRequest message;
        if req is ContextSubmitAssignmentRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("assignment01P01.assessmentManagement/request_assignments", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return <SubmitAssignmentResponse>result;
    }

    isolated remote function request_assignmentsContext(SubmitAssignmentRequest|ContextSubmitAssignmentRequest req) returns ContextSubmitAssignmentResponse|grpc:Error {
        map<string|string[]> headers = {};
        SubmitAssignmentRequest message;
        if req is ContextSubmitAssignmentRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("assignment01P01.assessmentManagement/request_assignments", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: <SubmitAssignmentResponse>result, headers: respHeaders};
    }

    isolated remote function submit_marks(SubmitMarksRequest|ContextSubmitMarksRequest req) returns string|grpc:Error {
        map<string|string[]> headers = {};
        SubmitMarksRequest message;
        if req is ContextSubmitMarksRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("assignment01P01.assessmentManagement/submit_marks", message, headers);
        [anydata, map<string|string[]>] [result, _] = payload;
        return result.toString();
    }

    isolated remote function submit_marksContext(SubmitMarksRequest|ContextSubmitMarksRequest req) returns wrappers:ContextString|grpc:Error {
        map<string|string[]> headers = {};
        SubmitMarksRequest message;
        if req is ContextSubmitMarksRequest {
            message = req.content;
            headers = req.headers;
        } else {
            message = req;
        }
        var payload = check self.grpcClient->executeSimpleRPC("assignment01P01.assessmentManagement/submit_marks", message, headers);
        [anydata, map<string|string[]>] [result, respHeaders] = payload;
        return {content: result.toString(), headers: respHeaders};
    }

    isolated remote function create_users() returns Create_usersStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("assignment01P01.assessmentManagement/create_users");
        return new Create_usersStreamingClient(sClient);
    }

    isolated remote function create_courses() returns Create_coursesStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("assignment01P01.assessmentManagement/create_courses");
        return new Create_coursesStreamingClient(sClient);
    }

    isolated remote function create_assessment() returns Create_assessmentStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("assignment01P01.assessmentManagement/create_assessment");
        return new Create_assessmentStreamingClient(sClient);
    }

    isolated remote function register() returns RegisterStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("assignment01P01.assessmentManagement/register");
        return new RegisterStreamingClient(sClient);
    }

    isolated remote function submit_assignments() returns Submit_assignmentsStreamingClient|grpc:Error {
        grpc:StreamingClient sClient = check self.grpcClient->executeClientStreaming("assignment01P01.assessmentManagement/submit_assignments");
        return new Submit_assignmentsStreamingClient(sClient);
    }
}

public client class Create_usersStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCreateUserRequest(CreateUserRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCreateUserRequest(ContextCreateUserRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Create_coursesStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCreateCourseRequest(CreateCourseRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCreateCourseRequest(ContextCreateCourseRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveCreateCourseResponse() returns CreateCourseResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return <CreateCourseResponse>payload;
        }
    }

    isolated remote function receiveContextCreateCourseResponse() returns ContextCreateCourseResponse|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: <CreateCourseResponse>payload, headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Create_assessmentStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCreateAssessmentRequest(CreateAssessmentRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCreateAssessmentRequest(ContextCreateAssessmentRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class RegisterStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendCreateCourseRequest(CreateCourseRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextCreateCourseRequest(ContextCreateCourseRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class Submit_assignmentsStreamingClient {
    private grpc:StreamingClient sClient;

    isolated function init(grpc:StreamingClient sClient) {
        self.sClient = sClient;
    }

    isolated remote function sendSubmitAssignmentRequest(SubmitAssignmentRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function sendContextSubmitAssignmentRequest(ContextSubmitAssignmentRequest message) returns grpc:Error? {
        return self.sClient->send(message);
    }

    isolated remote function receiveString() returns string|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, _] = response;
            return payload.toString();
        }
    }

    isolated remote function receiveContextString() returns wrappers:ContextString|grpc:Error? {
        var response = check self.sClient->receive();
        if response is () {
            return response;
        } else {
            [anydata, map<string|string[]>] [payload, headers] = response;
            return {content: payload.toString(), headers: headers};
        }
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.sClient->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.sClient->complete();
    }
}

public client class AssessmentManagementCreateCourseResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendCreateCourseResponse(CreateCourseResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextCreateCourseResponse(ContextCreateCourseResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class AssessmentManagementSubmitAssignmentResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendSubmitAssignmentResponse(SubmitAssignmentResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextSubmitAssignmentResponse(ContextSubmitAssignmentResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class AssessmentManagementStringCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendString(string response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextString(wrappers:ContextString response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public client class AssessmentManagementViewResponseCaller {
    private grpc:Caller caller;

    public isolated function init(grpc:Caller caller) {
        self.caller = caller;
    }

    public isolated function getId() returns int {
        return self.caller.getId();
    }

    isolated remote function sendViewResponse(ViewResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendContextViewResponse(ContextViewResponse response) returns grpc:Error? {
        return self.caller->send(response);
    }

    isolated remote function sendError(grpc:Error response) returns grpc:Error? {
        return self.caller->sendError(response);
    }

    isolated remote function complete() returns grpc:Error? {
        return self.caller->complete();
    }

    public isolated function isCancelled() returns boolean {
        return self.caller.isCancelled();
    }
}

public type ContextCreateCourseRequestStream record {|
    stream<CreateCourseRequest, error?> content;
    map<string|string[]> headers;
|};

public type ContextCreateAssessmentRequestStream record {|
    stream<CreateAssessmentRequest, error?> content;
    map<string|string[]> headers;
|};

public type ContextCreateUserRequestStream record {|
    stream<CreateUserRequest, error?> content;
    map<string|string[]> headers;
|};

public type ContextViewResponse record {|
    ViewResponse content;
    map<string|string[]> headers;
|};

public type ContextCreateCourseRequest record {|
    CreateCourseRequest content;
    map<string|string[]> headers;
|};

public type ContextCreateAssessmentRequest record {|
    CreateAssessmentRequest content;
    map<string|string[]> headers;
|};

public type ContextCreateCourseResponse record {|
    CreateCourseResponse content;
    map<string|string[]> headers;
|};

public type ContextCreateUserRequest record {|
    CreateUserRequest content;
    map<string|string[]> headers;
|};

public type ContextViewRequest record {|
    ViewRequest content;
    map<string|string[]> headers;
|};

public type ContextSubmitAssignmentResponse record {|
    SubmitAssignmentResponse content;
    map<string|string[]> headers;
|};

public type ContextSubmitMarksRequest record {|
    SubmitMarksRequest content;
    map<string|string[]> headers;
|};

public type ContextSubmitAssignmentRequest record {|
    SubmitAssignmentRequest content;
    map<string|string[]> headers;
|};

public type ViewResponse record {|
    SubmitMarksRequest assignmentMarks = {};
|};

public type CreateCourseRequest record {|
    string course_code = "";
    string course_name = "";
    CreateUserRequest course_assessor = {};
|};

public type CreateAssessmentRequest record {|
    string quantity = "";
    string assignment_id = "";
    string weight = "";
|};

public type CreateCourseResponse record {|
    string course_code = "";
|};

public type CreateUserRequest record {|
    string user_id = "";
    string name = "";
|};

public type ViewRequest record {|
    string course_code = "";
    string assignment_id = "";
|};

public type SubmitMarksRequest record {|
    string assignment_id = "";
    string marks = "";
|};

public type SubmitAssignmentResponse record {|
    string marked = "";
    string assignment_id = "";
|};

public type SubmitAssignmentRequest record {|
    string user_id = "";
    string assignment_id = "";
    string course_code = "";
|};

const string ROOT_DESCRIPTOR_PROTOCOL_BUFFER = "0A1570726F746F636F6C5F6275666665722E70726F746F120F61737369676E6D656E7430315030311A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22400A11437265617465557365725265717565737412170A07757365725F6964180120012809520675736572496412120A046E616D6518022001280952046E616D6522A4010A13437265617465436F7572736552657175657374121F0A0B636F757273655F636F6465180120012809520A636F75727365436F6465121F0A0B636F757273655F6E616D65180220012809520A636F757273654E616D65124B0A0F636F757273655F6173736573736F7218032001280B32222E61737369676E6D656E7430315030312E4372656174655573657252657175657374520E636F757273654173736573736F7222720A174372656174654173736573736D656E7452657175657374121A0A087175616E7469747918012001280552087175616E7469747912230A0D61737369676E6D656E745F6964180220012809520C61737369676E6D656E74496412160A06776569676874180320012809520677656967687422780A175375626D697441737369676E6D656E745265717565737412170A07757365725F6964180120012809520675736572496412230A0D61737369676E6D656E745F6964180220012809520C61737369676E6D656E744964121F0A0B636F757273655F636F6465180320012809520A636F75727365436F646522530A0B5669657752657175657374121F0A0B636F757273655F636F6465180120012809520A636F75727365436F646512230A0D61737369676E6D656E745F6964180220012809520C61737369676E6D656E744964224F0A125375626D69744D61726B735265717565737412230A0D61737369676E6D656E745F6964180120012809520C61737369676E6D656E74496412140A056D61726B7318022001280952056D61726B7322370A14437265617465436F75727365526573706F6E7365121F0A0B636F757273655F636F6465180120012809520A636F75727365436F6465225D0A0C56696577526573706F6E7365124D0A0F61737369676E6D656E744D61726B7318012001280B32232E61737369676E6D656E7430315030312E5375626D69744D61726B7352657175657374520F61737369676E6D656E744D61726B7322570A185375626D697441737369676E6D656E74526573706F6E736512160A066D61726B656418012001280952066D61726B656412230A0D61737369676E6D656E745F6964180220012809520C61737369676E6D656E74496432BE060A146173736573736D656E744D616E6167656D656E7412520A0C6372656174655F757365727312222E61737369676E6D656E7430315030312E43726561746555736572526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75652801125F0A0E6372656174655F636F757273657312242E61737369676E6D656E7430315030312E437265617465436F75727365526571756573741A252E61737369676E6D656E7430315030312E437265617465436F75727365526573706F6E73652801125D0A116372656174655F6173736573736D656E7412282E61737369676E6D656E7430315030312E4372656174654173736573736D656E74526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565280112540A0E61737369676E5F636F757273657312242E61737369676E6D656E7430315030312E437265617465436F75727365526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512500A08726567697374657212242E61737369676E6D656E7430315030312E437265617465436F75727365526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75652801125E0A127375626D69745F61737369676E6D656E747312282E61737369676E6D656E7430315030312E5375626D697441737369676E6D656E74526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75652801124B0A0C766965775F726573756C7473121C2E61737369676E6D656E7430315030312E56696577526571756573741A1D2E61737369676E6D656E7430315030312E56696577526573706F6E7365126A0A13726571756573745F61737369676E6D656E747312282E61737369676E6D656E7430315030312E5375626D697441737369676E6D656E74526571756573741A292E61737369676E6D656E7430315030312E5375626D697441737369676E6D656E74526573706F6E736512510A0C7375626D69745F6D61726B7312232E61737369676E6D656E7430315030312E5375626D69744D61726B73526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565620670726F746F33";

public isolated function getDescriptorMapProtocolBuffer() returns map<string> {
    return {"google/protobuf/wrappers.proto": "0A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F120F676F6F676C652E70726F746F62756622230A0B446F75626C6556616C756512140A0576616C7565180120012801520576616C756522220A0A466C6F617456616C756512140A0576616C7565180120012802520576616C756522220A0A496E74363456616C756512140A0576616C7565180120012803520576616C756522230A0B55496E74363456616C756512140A0576616C7565180120012804520576616C756522220A0A496E74333256616C756512140A0576616C7565180120012805520576616C756522230A0B55496E74333256616C756512140A0576616C756518012001280D520576616C756522210A09426F6F6C56616C756512140A0576616C7565180120012808520576616C756522230A0B537472696E6756616C756512140A0576616C7565180120012809520576616C756522220A0A427974657356616C756512140A0576616C756518012001280C520576616C756542570A13636F6D2E676F6F676C652E70726F746F627566420D577261707065727350726F746F50015A057479706573F80101A20203475042AA021E476F6F676C652E50726F746F6275662E57656C6C4B6E6F776E5479706573620670726F746F33", "protocol_buffer.proto": "0A1570726F746F636F6C5F6275666665722E70726F746F120F61737369676E6D656E7430315030311A1E676F6F676C652F70726F746F6275662F77726170706572732E70726F746F22400A11437265617465557365725265717565737412170A07757365725F6964180120012809520675736572496412120A046E616D6518022001280952046E616D6522A4010A13437265617465436F7572736552657175657374121F0A0B636F757273655F636F6465180120012809520A636F75727365436F6465121F0A0B636F757273655F6E616D65180220012809520A636F757273654E616D65124B0A0F636F757273655F6173736573736F7218032001280B32222E61737369676E6D656E7430315030312E4372656174655573657252657175657374520E636F757273654173736573736F7222720A174372656174654173736573736D656E7452657175657374121A0A087175616E7469747918012001280552087175616E7469747912230A0D61737369676E6D656E745F6964180220012809520C61737369676E6D656E74496412160A06776569676874180320012809520677656967687422780A175375626D697441737369676E6D656E745265717565737412170A07757365725F6964180120012809520675736572496412230A0D61737369676E6D656E745F6964180220012809520C61737369676E6D656E744964121F0A0B636F757273655F636F6465180320012809520A636F75727365436F646522530A0B5669657752657175657374121F0A0B636F757273655F636F6465180120012809520A636F75727365436F646512230A0D61737369676E6D656E745F6964180220012809520C61737369676E6D656E744964224F0A125375626D69744D61726B735265717565737412230A0D61737369676E6D656E745F6964180120012809520C61737369676E6D656E74496412140A056D61726B7318022001280952056D61726B7322370A14437265617465436F75727365526573706F6E7365121F0A0B636F757273655F636F6465180120012809520A636F75727365436F6465225D0A0C56696577526573706F6E7365124D0A0F61737369676E6D656E744D61726B7318012001280B32232E61737369676E6D656E7430315030312E5375626D69744D61726B7352657175657374520F61737369676E6D656E744D61726B7322570A185375626D697441737369676E6D656E74526573706F6E736512160A066D61726B656418012001280952066D61726B656412230A0D61737369676E6D656E745F6964180220012809520C61737369676E6D656E74496432BE060A146173736573736D656E744D616E6167656D656E7412520A0C6372656174655F757365727312222E61737369676E6D656E7430315030312E43726561746555736572526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75652801125F0A0E6372656174655F636F757273657312242E61737369676E6D656E7430315030312E437265617465436F75727365526571756573741A252E61737369676E6D656E7430315030312E437265617465436F75727365526573706F6E73652801125D0A116372656174655F6173736573736D656E7412282E61737369676E6D656E7430315030312E4372656174654173736573736D656E74526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565280112540A0E61737369676E5F636F757273657312242E61737369676E6D656E7430315030312E437265617465436F75727365526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C756512500A08726567697374657212242E61737369676E6D656E7430315030312E437265617465436F75727365526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75652801125E0A127375626D69745F61737369676E6D656E747312282E61737369676E6D656E7430315030312E5375626D697441737369676E6D656E74526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C75652801124B0A0C766965775F726573756C7473121C2E61737369676E6D656E7430315030312E56696577526571756573741A1D2E61737369676E6D656E7430315030312E56696577526573706F6E7365126A0A13726571756573745F61737369676E6D656E747312282E61737369676E6D656E7430315030312E5375626D697441737369676E6D656E74526571756573741A292E61737369676E6D656E7430315030312E5375626D697441737369676E6D656E74526573706F6E736512510A0C7375626D69745F6D61726B7312232E61737369676E6D656E7430315030312E5375626D69744D61726B73526571756573741A1C2E676F6F676C652E70726F746F6275662E537472696E6756616C7565620670726F746F33"};
}

